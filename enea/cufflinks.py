import cufflinks as cf
cf.go_offline()

# Define new color scale
colorsccale = [ 'rgb(80,190,120)',
                'rgb(245,143,27)',
                'rgb(247,198,26)',
                'rgb(80,123,138)',
                'rgb(231,76,60)',
                'rgb(125,120,112)',
                'rgb(72,72,72)']
cf.colors._custom_scales['qual']['enea'] = colorsccale
cf.colors.reset_scales()

# Define new themes
enea = {
    'colorscale':'enea',
    'linewidth':2,
    'linecolor':'pearl',
    'bargap':.01,
    'layout':{
        'legend' : {'bgcolor':'rgba(0,0,0,0)','font':{'color':'grey10'}},
        'paper_bgcolor' : 'rgba(0,0,0,0)',
        'plot_bgcolor' : 'rgba(0,0,0,0)',
        'yaxis' : {
            'tickfont' : {'color':'grey10'},
            'gridcolor' : 'grey10',
            'titlefont' : {'color':'grey10'},
            'zerolinecolor' : 'grey10',
            'showgrid' : True
        },
        'xaxis' : {
            'tickfont' : {'color':'grey10'},
            'gridcolor' : 'grey10',
            'titlefont' : {'color':'grey10'},
            'zerolinecolor' : 'grey10',
            'showgrid' : False
        },
        'titlefont' : {'color':'charcoal'}
    },
    'annotations' : {
        'fontcolor' : 'grey10',
        'arrowcolor' : 'grey10'
    }
}

enea_print = {
    'colorscale':'enea',
    'linewidth':2,
    'linecolor':'pearl',
    'bargap':.01,
    'layout':{
        'legend' : {'bgcolor':'rgba(0,0,0,0)','font':{'color':'grey10'}},
        'paper_bgcolor' : '#fff',
        'plot_bgcolor' : '#fff',
        'yaxis' : {
            'tickfont' : {'color':'grey10'},
            'gridcolor' : 'grey10',
            'titlefont' : {'color':'grey10'},
            'zerolinecolor' : 'grey10',
            'showgrid' : True
        },
        'xaxis' : {
            'tickfont' : {'color':'grey10'},
            'gridcolor' : 'grey10',
            'titlefont' : {'color':'grey10'},
            'zerolinecolor' : 'grey10',
            'showgrid' : False
        },
        'titlefont' : {'color':'charcoal'}
    },
    'annotations' : {
        'fontcolor' : 'grey10',
        'arrowcolor' : 'grey10'
    }
}

# Add the themes to cufflinks
cf.themes.THEMES['enea'] = enea
cf.themes.THEMES['enea_print'] = enea_print

# Set enea as default theme
cf.set_config_file(theme='enea') 