import pandas as pd

def downsample(df, window=11):
    """Helper function to downsample a graph while keeping the envelope shape

    Parameters
    ----------
    df : pd.DataFrame OR pd.Series
        The data to downsample
    window : int, optional
        The size of the rolling window for the downsampling (the default is 11, which is the optimal for 30 minute time series loads)

    Returns
    -------
    pd.DataFrame
        The downsampled dataframe
    """
    q = .1
    def downsample_series(df, window):
        if window == 1:
            return df
        df = df.dropna()
        df_padded = pd.concat([pd.Series(data=df.iat[0], index=range(int(window/2))), df,
                               pd.Series(data=df.iat[-1], index=range(int(window/2)))],
                              axis=0)
        df_downsampled = df_padded.loc[(df_padded > df_padded.rolling(window, center=True).quantile(1 - q)) |
                                       (df_padded < df_padded.rolling(window, center=True).quantile(q))]
        if df_downsampled.index[0] > df.index[0]:
            df_downsampled = pd.concat([df.iloc[[0]], df_downsampled], axis=0)
        if df_downsampled.index[-1] < df.index[-1]:
            df_downsampled = pd.concat([df_downsampled, df.iloc[[-1]]], axis=0)
        return df_downsampled
    if isinstance(df, pd.DataFrame):
        idx = pd.Index([])
        for col in df:
            idx = idx.append(downsample_series(df[col], window).index)
        return df.loc[idx.unique().sort_values()]
    elif isinstance(df, pd.Series):
        return downsample_series(df, window)

# Monkeypatch DataFrame and Series to support downsampling 
pd.DataFrame.downsample = downsample
pd.Series.downsample = downsample

# Monkeypatch DataFrame, Series and DatetimIndex to support year "unleaping"
pd.DatetimeIndex.unleap = lambda idx: idx[~((idx.day == 29) & (idx.month == 2))]
pd.DataFrame.unleap = lambda time_serie : time_serie.loc[~((time_serie.index.day == 29) & (time_serie.index.month == 2))]
pd.Series.unleap = lambda time_serie : time_serie.loc[~((time_serie.index.day == 29) & (time_serie.index.month == 2))]