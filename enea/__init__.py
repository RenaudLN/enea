import warnings

try:
    from .cufflinks import cf
except ModuleNotFoundError:
    warnings.warn('Cufflinks package was not found and could not be customised')

try:
    from .pandas import pd
except ModuleNotFoundError:
    warnings.warn('Pandas package was not found and could not be customised')
