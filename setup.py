from setuptools import find_packages, setup

setup(
    name = 'ENEA',
    version = '1.0.0',
    url = 'https://RenaudLN@bitbucket.org/RenaudLN/enea.git',
    author = 'Renaud Laine',
    author_email = 'renaud.laine@enea-consulting.com',
    description = 'Customising standard packages (cufflinks, pandas)',
    packages = find_packages(),    
)