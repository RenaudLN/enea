# ENEA library customisation

This module is used to overload some of the libraries currently used, adding custom themes and functionalities to said libraries

## Installation

### Prerequisites

enea modifies the following libraries:

* pandas
* cufflinks

### User installation

To install enea run the following line on your git command line:

```
pip install git+https://RenaudLN@bitbucket.org/RenaudLN/enea.git
```

### Usage

To customise the libraries, run:

```
import enea
```

## Package content

### Pandas add-ons

The following functionalities are added to pandas:

* downsample() for pd.DataFrame and pd.Series, this reduces the number of data points without degrading the shape of the signal, useful for plotting large data
* unleap() for pd.DataFrame, pd.Series and pd.DatetimeIndex, this removes the 29th of February in the index

### Cufflinks customisation

Two themes are added to cufflinks:

* enea, set as default theme, enea color-scheme and transparent background
* enea_print, enea-color scheme, white background and larger fonts, used to take screenshots for presentations

cufflinks is set to work offline.

## Authors

* **Renaud Lainé**

See also the list of [contributors](https://bitbucket.org/RenaudLN/enea/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
